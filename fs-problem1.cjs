const fs = require("fs");

function deleteFiles(dirName, files) {
  console.log(files.length);
  for (let index = 0; index < files.length; index++) {
    fs.unlink(`${dirName}/${files[index]}.json`, (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log("deleted file successfully");
      }
    });
  }
}

let count = 0;

function writeFiles(dirName, number) {
  const content = JSON.stringify("Hello World!");

  const arrayOfFileNames = [];

  for (let index = 1; index <= number; index++) {
    const fileName = `file_${index}`;

    arrayOfFileNames.push(fileName);

    fs.writeFile(`${dirName}/${fileName}.json`, content, (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log("creating file success");
        count++;
        if (count === number) {
          console.log(arrayOfFileNames, "writefile");
          deleteFiles(dirName, arrayOfFileNames);
        }
      }
    });
  }
}

function createRandomFilesAndDelete(dirName, number) {
  fs.mkdir(dirName, (err) => {
    if (err) {
      console.log(err);
    } else {
      writeFiles(dirName, number);
    }
  });
}

module.exports = createRandomFilesAndDelete;
