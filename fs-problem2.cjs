const fs = require("fs");
const path = require("path");
const dirname = "/home/kavya/Downloads/";
const filePath = path.join(dirname, "lipsum.txt");
const newFile1Path = path.join(__dirname, "newfile1.txt");
const filenamesPath = path.join(__dirname, "filenames.txt");
const newFile2Path = path.join(__dirname, "newfile2.txt");
const newFile3Path = path.join(__dirname, "newFile3.txt");
console.log(filePath);



function problem2() {

  fs.readFile(filePath, "utf8", (err, data) => {
    if (err) {
      console.log(err);
    } else {
      data = data.toUpperCase();
      console.log("read the file and converted to upperCase");

      fs.writeFile(newFile1Path, data, "utf8", (err) => {
        if (err) {
          console.log(err);
        } else {
          let fNameArray = newFile1Path.split("/");
          console.log(fNameArray);
          const fName = fNameArray[fNameArray.length - 1] + "\n";

          fs.writeFile(filenamesPath, fName, "utf8", (err) => {
            if (err) {
              console.log(err);
            } else {
              console.log("saved the uppercased data");

              fs.readFile(newFile1Path, "utf8", (err, data) => {
                if (err) {
                  console.log(err);
                } else {
                  data = data.toLowerCase().split(".");
                  data = JSON.stringify(data);
                  console.log("read the file and converted to lower case");

                  fs.writeFile(newFile2Path, data, (err) => {
                    if (err) {
                      console.log(err);
                    } else {
                      let fNameArray = newFile2Path.split("/");
                      const fName = fNameArray[fNameArray.length - 1] + "\n";

                      fs.appendFile(filenamesPath, fName, "utf8", (err) => {
                        if (err) {
                          console.log(err);
                        } else {
                          console.log("saved the files name");

                          fs.readFile(newFile2Path, "utf8", (err, data) => {
                            if (err) {
                              console.log(err);
                            } else {
                              data = JSON.parse(data).sort();
                              data = JSON.stringify(data);
                              console.log("sorted the data");

                              fs.writeFile(newFile3Path, data, (err) => {
                                if (err) {
                                  console.log(err);
                                } else {
                                  let fNameArray = newFile3Path.split("/");
                                  const fName =
                                    fNameArray[fNameArray.length - 1] + "\n";

                                  fs.appendFile(
                                    filenamesPath,
                                    fName,
                                    "utf8",
                                    (err) => {
                                      if (err) {
                                        console.log(err);
                                      } else {
                                        
                                        fs.readFile(
                                          filenamesPath,
                                          "utf8",
                                          (err, data) => {
                                            if (err) {
                                              console.log(err);
                                            } else {
                                              data = data.split("\n");
                                              data.pop();

                                              data.map((file) => {
                                                const filePath = path.join(
                                                  __dirname,
                                                  file
                                                );

                                                fs.unlink(filePath, (err) => {
                                                  if (err) {
                                                    console.log(err);
                                                  } else {
                                                    console.log(
                                                      "deleted the file successfully"
                                                    );
                                                  }
                                                });
                                              });
                                            }
                                          }
                                        );
                                      }
                                    }
                                  );
                                }
                              });
                            }
                          });
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
}

module.exports = problem2;
